package ir.co.ariapardaz.alarm.Activity.Model;

import org.androidannotations.annotations.EBean;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by m on 17/09/2019.
 */

public class EventModel extends RealmObject {



    @PrimaryKey
    int id;

    String Event,Time,soundid,periodid,Year,day,month,houre;
    int  TimeDetail;
    boolean offon,notif_sw,sound_sw,Period_sw;

    public EventModel() {
    }

    public EventModel(int id, String event, String time, String soundid, String periodid, String year, String day, String month,String houre, int timeDetail,
                      boolean offon, boolean notif_sw, boolean sound_sw, boolean period_sw) {
        this.id = id;
        Event = event;
        Time = time;
        this.soundid = soundid;
        this.periodid = periodid;
        Year = year;
        this.day = day;
        this.month = month;
        this.houre = houre;
        TimeDetail = timeDetail;
        this.offon = offon;
        this.notif_sw = notif_sw;
        this.sound_sw = sound_sw;
        Period_sw = period_sw;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvent() {
        return Event;
    }

    public void setEvent(String event) {
        Event = event;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getSoundid() {
        return soundid;
    }

    public void setSoundid(String soundid) {
        this.soundid = soundid;
    }

    public String getPeriodid() {
        return periodid;
    }

    public void setPeriodid(String periodid) {
        this.periodid = periodid;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public String gethoure() {
        return houre;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void sethoure(String houre) { this.houre = houre; }

    public int getTimeDetail() {
        return TimeDetail;
    }

    public void setTimeDetail(int timeDetail) {
        TimeDetail = timeDetail;
    }

    public boolean isOffon() {
        return offon;
    }

    public void setOffon(boolean offon) {
        this.offon = offon;
    }

    public boolean isNotif_sw() {
        return notif_sw;
    }

    public void setNotif_sw(boolean notif_sw) {
        this.notif_sw = notif_sw;
    }

    public boolean isSound_sw() {
        return sound_sw;
    }

    public void setSound_sw(boolean sound_sw) {
        this.sound_sw = sound_sw;
    }

    public boolean isPeriod_sw() {
        return Period_sw;
    }

    public void setPeriod_sw(boolean period_sw) {
        Period_sw = period_sw;
    }

    @Override
    public String toString() {
        return "EventModel{" +
                "id=" + id +
                ", Event='" + Event + '\'' +
                ", Time='" + Time + '\'' +
                ", soundid='" + soundid + '\'' +
                ", periodid='" + periodid + '\'' +
                ", Year='" + Year + '\'' +
                ", day='" + day + '\'' +
                ", month='" + month + '\'' +
                ", houre='" + houre + '\'' +
                ", TimeDetail=" + TimeDetail +
                ", offon=" + offon +
                ", notif_sw=" + notif_sw +
                ", sound_sw=" + sound_sw +
                ", Period_sw=" + Period_sw +
                '}';
    }
}
