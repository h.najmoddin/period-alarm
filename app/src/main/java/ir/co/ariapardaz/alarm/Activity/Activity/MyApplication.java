package ir.co.ariapardaz.alarm.Activity.Activity;

import android.app.Application;

import com.orhanobut.hawk.Hawk;

import io.realm.Realm;

/**
 * Created by m on 2018/12/02.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Hawk.init(this).build();
        Realm.init(this);
    }

    @Override
    public void onTerminate() {

        super.onTerminate();
    }





}
