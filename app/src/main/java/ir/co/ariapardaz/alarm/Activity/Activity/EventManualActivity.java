package ir.co.ariapardaz.alarm.Activity.Activity;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;
import com.orhanobut.hawk.Hawk;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.Calendar;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

import ir.co.ariapardaz.alarm.Activity.Adapter.EventAdepter;
import ir.co.ariapardaz.alarm.Activity.DateConverter;
import ir.co.ariapardaz.alarm.Activity.DetailClass;
import ir.co.ariapardaz.alarm.Activity.Model.EventModel;
import ir.co.ariapardaz.alarm.Activity.Services.EventService;
import ir.co.ariapardaz.alarm.Activity.Services.MyReceiver;
import ir.co.ariapardaz.alarm.R;


@EActivity(R.layout.activity_event_manual)
public class EventManualActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    String[] Temps = new String[3];
    Context context = this;
    public int idcode;
    final RealmConfiguration eventConfig = new RealmConfiguration.Builder().name("evntrealm.realm")
            .schemaVersion(3).build();
    final Realm Evrealm = Realm.getInstance(eventConfig);
    private RealmResults<EventModel> result;
    EventModel m1;

    @Bean
    DetailClass DC;
    @ViewById
    RecyclerView Event_rc;
    @ViewById
    SwipeRefreshLayout refreshEvent;


    @Click
    void Fab2() {
        AddEventDialog();

    }

    @AfterViews
    void AF() {

        EventAdepter articleAdapter = new EventAdepter(context);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(context,
                LinearLayoutManager.VERTICAL, false);

        Event_rc.setLayoutManager(horizontalLayoutManagaer);
        Event_rc.setAdapter(articleAdapter);

        refreshEvent.setOnRefreshListener(this);
    }

    public void PeriodEdit(int X, boolean s) {


        idcode = X;
        Evrealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm Evrealm) {
                m1 = Evrealm.where(EventModel.class).equalTo("id", idcode).findFirst();
            }
        });


        DateConverter converter = new DateConverter();
        converter.persianToGregorian(Integer.parseInt(m1.getYear()), Integer.parseInt(m1.getMonth()), Integer.parseInt(m1.getDay()));
        Toast.makeText(this, converter.getYear() + "/" + converter.getMonth() + "/" + converter.getDay(), Toast.LENGTH_LONG).show(); //

        start();
        //EditClockpDialog(m1, s);

    }

    public void PeriodSwitch(int X) {
        idcode = X;
        EventModel result = Evrealm.where(EventModel.class).equalTo("id", idcode).findFirst();
        boolean sw = result.isOffon();
        if (sw) {

            Evrealm.beginTransaction();
            result.setOffon(false);
            Evrealm.commitTransaction();
            // cancels(X);
        } else {
            Evrealm.beginTransaction();
            result.setOffon(true);
            Evrealm.commitTransaction();
            //  Start(result.getTimeDetail(), result.getId(), result.getDetail());
        }

    }

    private void AddEventDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_add_event, null);
        final EditText edtserial, houre_edt;
        final TextView year_txt, month_txt, day_txt;
        final Switch notif_sw, sound_sw, Period_sw;
        final Spinner SoundSpiner, PeriodSpiner;
        ImageView Calender_img;

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogDanger));
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("ADD", null);
        alertDialogBuilder.setNegativeButton("CANCEL", null);

        year_txt = (TextView) view.findViewById(R.id.year_txt);
        month_txt = (TextView) view.findViewById(R.id.month_txt);
        day_txt = (TextView) view.findViewById(R.id.day_txt);
        edtserial = (EditText) view.findViewById(R.id.edtserial);
        houre_edt = (EditText) view.findViewById(R.id.houre_edt);
        notif_sw = (Switch) view.findViewById(R.id.notif_sw);
        sound_sw = (Switch) view.findViewById(R.id.sound_sw);
        Period_sw = (Switch) view.findViewById(R.id.Period_sw);
        SoundSpiner = (Spinner) view.findViewById(R.id.SoundSpiner);
        PeriodSpiner = (Spinner) view.findViewById(R.id.PeriodSpiner);
        Calender_img = (ImageView) view.findViewById(R.id.Calender_img);

        ArrayAdapter<String> SpinerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.SoundList));
        SpinerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SoundSpiner.setAdapter(SpinerAdapter);

        SpinerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.periodList));
        SpinerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        PeriodSpiner.setAdapter(SpinerAdapter);

        Calender_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PersianCalendar now = new PersianCalendar();
                final DatePickerDialog datePickerDialog =
                        DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                         @Override
                                                         public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                             year_txt.setText(year + "");
                                                             month_txt.setText((monthOfYear + 1) + "");
                                                             day_txt.setText(dayOfMonth + "");
                                                             DC.ToastTest("" + year + "/" + monthOfYear + "/" + dayOfMonth);
                                                         }
                                                     }, now.getPersianYear(),
                                now.getPersianMonth(),
                                now.getPersianDay());
                datePickerDialog.setThemeDark(true);
                datePickerDialog.show(getFragmentManager(), "tpd");
                datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Toasty.warning(context, "تاریخ مورد نظر انتخاب نشد", Toast.LENGTH_LONG, true).show();
                    }
                });
            }
        });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                btnPositive.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onClick(View v) {
                        String SoundMode = SoundSpiner.getSelectedItem().toString();
                        String periodMode = PeriodSpiner.getSelectedItem().toString();
                        String Houre = "12";
                        if (!(year_txt.getText().toString().equals("سال"))) {
                            if (!(TextUtils.isEmpty(edtserial.getText()))) {
                                int code = getNextKey();
                                if (!(TextUtils.isEmpty(houre_edt.getText())))
                                    Houre = houre_edt.getText().toString();
                                save(edtserial.getText().toString(), "notset", SoundMode, periodMode, year_txt.getText().toString(), month_txt.getText().toString(),
                                        day_txt.getText().toString(), Houre, true, notif_sw.isChecked(), sound_sw.isChecked(), Period_sw.isChecked()
                                        , 0, code);

//                                start(year_txt.getText().toString(), month_txt.getText().toString(), day_txt.getText().toString(),
//                                        Houre, code, edtserial.getText().toString());
                                finish();
                                startActivity(getIntent());
                                Toasty.success(context, "رویداد مورد نظر ثبت گردید", Toast.LENGTH_LONG, true).show();
                                alertDialog.dismiss();
                            } else {
                                Toasty.warning(context, "عنوان رویداد مشخص نشده", Toast.LENGTH_LONG, true).show();
                            }

                        } else {
                            Toasty.warning(context, "تاریخ رویداد مشخص نشده", Toast.LENGTH_LONG, true).show();
                        }
                    }
                });
                Button btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                btnNegative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toasty.warning(context, "انصراف از ثبت رویداد !!", Toast.LENGTH_LONG, true).show();
                        alertDialog.dismiss();
                    }
                });
            }
        });
        alertDialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_linear));
        alertDialog.show();
        int width = Hawk.get("width");
        width = (int) ((width) * ((double) 4 / 5));
        alertDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    private void EditClockpDialog(final EventModel EV, final boolean switchoffon) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_add_event, null);
        final EditText edtserial, houre_edt;
        final TextView year_txt, month_txt, day_txt;
        final Switch notif_sw, sound_sw, Period_sw;
        final Spinner SoundSpiner, PeriodSpiner;
        ImageView Calender_img;

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogDanger));
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Edit", null);
        alertDialogBuilder.setNegativeButton("DELET", null);
        year_txt = (TextView) view.findViewById(R.id.year_txt);
        month_txt = (TextView) view.findViewById(R.id.month_txt);
        day_txt = (TextView) view.findViewById(R.id.day_txt);
        edtserial = (EditText) view.findViewById(R.id.edtserial);
        houre_edt = (EditText) view.findViewById(R.id.houre_edt);
        notif_sw = (Switch) view.findViewById(R.id.notif_sw);
        sound_sw = (Switch) view.findViewById(R.id.sound_sw);
        Period_sw = (Switch) view.findViewById(R.id.Period_sw);
        SoundSpiner = (Spinner) view.findViewById(R.id.SoundSpiner);
        PeriodSpiner = (Spinner) view.findViewById(R.id.PeriodSpiner);
        Calender_img = (ImageView) view.findViewById(R.id.Calender_img);

        ArrayAdapter<String> SoundAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.SoundList));
        SoundAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SoundSpiner.setAdapter(SoundAdapter);
        int spinnerSoundPosition = SoundAdapter.getPosition(EV.getSoundid());

        ArrayAdapter<String> PeriodAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.SoundList));
        SoundAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SoundSpiner.setAdapter(SoundAdapter);
        int spinnerPeriodPosition = PeriodAdapter.getPosition(EV.getPeriodid());
        year_txt.setText(EV.getYear());
        month_txt.setText(EV.getMonth());
        day_txt.setText(EV.getDay());
        edtserial.setText(EV.getEvent());
        houre_edt.setText(EV.gethoure());
        notif_sw.setChecked(EV.isNotif_sw());
        sound_sw.setChecked(EV.isSound_sw());
        Period_sw.setChecked(EV.isPeriod_sw());
        SoundSpiner.setSelection(spinnerSoundPosition);
        PeriodSpiner.setSelection(spinnerPeriodPosition);


        Calender_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PersianCalendar now = new PersianCalendar();
                final DatePickerDialog datePickerDialog =
                        DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                         @Override
                                                         public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                             year_txt.setText(year + "");
                                                             month_txt.setText((monthOfYear + 1) + "");
                                                             day_txt.setText(dayOfMonth + "");
                                                             DC.ToastTest("" + year + "/" + monthOfYear + "/" + dayOfMonth);
                                                         }
                                                     }, now.getPersianYear(),
                                now.getPersianMonth(),
                                now.getPersianDay());
                datePickerDialog.setThemeDark(true);
                datePickerDialog.show(getFragmentManager(), "tpd");
                datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Toasty.warning(context, "تاریخ مورد نظر انتخاب نشد", Toast.LENGTH_LONG, true).show();
                    }
                });
            }
        });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                btnPositive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String SoundMode = SoundSpiner.getSelectedItem().toString();
                        String periodMode = PeriodSpiner.getSelectedItem().toString();
                        String Houre = "12";
                        if (!(year_txt.getText().toString().equals("سال"))) {
                            if (!(TextUtils.isEmpty(edtserial.getText()))) {
                                int code = EV.getId();

                                Edite(edtserial.getText().toString(), "notset", SoundMode, periodMode, year_txt.getText().toString(), month_txt.getText().toString(),
                                        day_txt.getText().toString(), Houre, switchoffon, notif_sw.isChecked(), sound_sw.isChecked(), Period_sw.isChecked()
                                        , 0, code);
                                finish();
                                startActivity(getIntent());
                                Toasty.success(context, "رویداد مورد نظر ثبت گردید", Toast.LENGTH_LONG, true).show();
                                alertDialog.dismiss();
                            } else {
                                Toasty.warning(context, "عنوان رویداد مشخص نشده", Toast.LENGTH_LONG, true).show();
                            }

                        } else {
                            Toasty.warning(context, "تاریخ رویداد مشخص نشده", Toast.LENGTH_LONG, true).show();
                        }
                    }
                });
                Button btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                btnNegative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DeletClock(idcode);
                        Toasty.error(context, "دوره زمانی مورد نظر پاک شد", Toast.LENGTH_LONG, true).show();
                        alertDialog.dismiss();
                    }
                });
            }
        });
        alertDialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_linear));
        alertDialog.show();
        int width = Hawk.get("width");
        width = (int) ((width) * ((double) 4 / 5));
        alertDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
    }


    public String[] GetTimePersian() {
        PersianCalendar now = new PersianCalendar();
        final DatePickerDialog datePickerDialog =
                DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                 @Override
                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                                                     Temps[0] = year + "";
                                                     Temps[1] = monthOfYear + "";
                                                     Temps[2] = dayOfMonth + "";
                                                     DC.ToastTest("" + year + "/" + monthOfYear + "/" + dayOfMonth);
                                                 }
                                             }, now.getPersianYear(),
                        now.getPersianMonth(),
                        now.getPersianDay());
        datePickerDialog.setThemeDark(true);
        datePickerDialog.show(getFragmentManager(), "tpd");


        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("TimePicker", "Dialog was cancelled");
            }
        });


        return Temps;
    }

    void save(String event, String time, String soundid, String periodid, String year, String month, String day, String houre,
              boolean offon, boolean notif_sw, boolean sound_sw, boolean period_sw, int TimeDetail, int idcode) {

        Evrealm.beginTransaction();
        final EventModel model = Evrealm.createObject(EventModel.class, idcode);
        model.setEvent(event);
        model.setTime(time);
        model.setSoundid(soundid);
        model.setPeriodid(periodid);
        model.setYear(year);
        model.setMonth(month);
        model.setDay(day);
        model.sethoure(houre);
        model.setOffon(offon);
        model.setNotif_sw(notif_sw);
        model.setSound_sw(sound_sw);
        model.setPeriod_sw(period_sw);
        model.setTimeDetail(TimeDetail);
        Evrealm.commitTransaction();

    }

    void Edite(String event, String time, String soundid, String periodid, String year, String month, String day, String houre,
               boolean offon, boolean notif_sw, boolean sound_sw, boolean period_sw, int TimeDetail, int idcode) {

        EventModel model = Evrealm.where(EventModel.class).equalTo("id", idcode).findFirst();
        Evrealm.beginTransaction();
        model.setEvent(event);
        model.setTime(time);
        model.setSoundid(soundid);
        model.setPeriodid(periodid);
        model.setYear(year);
        model.setMonth(month);
        model.setDay(day);
        model.sethoure(houre);
        model.setOffon(offon);
        model.setNotif_sw(notif_sw);
        model.setSound_sw(sound_sw);
        model.setPeriod_sw(period_sw);
        model.setTimeDetail(TimeDetail);
        Evrealm.commitTransaction();

    }

    public int getNextKey() {

        try {
            Number number = Evrealm.where(EventModel.class).max("id");
            if (number != null) {
                return number.intValue() + 1;
            } else {
                return 0;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }
    }

    @Override
    public void onRefresh() {

        finish();
        startActivity(getIntent());
        refreshEvent.setRefreshing(false);
    }




//    private void start(String Year, String month, String day, String houre, int codere, String Mtn) {
        private void start() {
            Toast.makeText(context, "starttt", Toast.LENGTH_SHORT).show();
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR,2019);
        cal.set(Calendar.MONTH,9);
        cal.set(Calendar.DAY_OF_MONTH,26);
        cal.set(Calendar.HOUR_OF_DAY,19);
        cal.set(Calendar.MINUTE,15);
        cal.set(Calendar.SECOND,00);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        Intent i = new Intent(this, MyReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(this, 1, i, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pi);
           // alarmManager.cancel(pi);
        }
        else{
            Toast.makeText(context, "not version", Toast.LENGTH_SHORT).show();
        }

    }


    public void DeletClock(int X) {
        idcode = X;
        //cancels(X);
        Evrealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm erealm) {
                RealmResults<EventModel> result = erealm.where(EventModel.class).equalTo("id", idcode).findAll();
                result.deleteAllFromRealm();
            }
        });

        finish();

        startActivity(getIntent());
    }

}
