package ir.co.ariapardaz.alarm.Activity.Activity;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.LinearLayout;

import com.orhanobut.hawk.Hawk;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.ViewById;

import ir.co.ariapardaz.alarm.R;


@EActivity(R.layout.activity_main_page)
public class MainPageActivity extends AppCompatActivity {

    Context context = this;


    @ViewById
    Button One_btn;
    @ViewById
    Button Two_btn;



    @AfterViews
    void af() {
        int width = Hawk.get("width");
       // Typeface byekan = Typeface.createFromAsset(context.getAssets(), "byekan.ttf");
        Typeface COOPBL = Typeface.createFromAsset(context.getAssets(), "COOPBL.ttf");
        Typeface ITCKRIST = Typeface.createFromAsset(context.getAssets(), "ITCKRIST.ttf");
        //LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width * 4 / 6, 80);
        One_btn.setTextSize(16);
        One_btn.setTypeface(ITCKRIST);
        Two_btn.setTextSize(16);
        Two_btn.setTypeface(ITCKRIST);
               //********************************************************//
        One_btn.setWidth(width * 3 / 4);
        Two_btn.setWidth(width * 3 / 4);


    }

    @Click
    void One_btn (){
        PeriodClockActivity_.intent(context).start();
    }

    @Click
    void Two_btn (){
        EventManualActivity_.intent(context).start();
    }




}
