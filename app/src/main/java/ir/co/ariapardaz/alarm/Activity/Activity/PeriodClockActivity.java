package ir.co.ariapardaz.alarm.Activity.Activity;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;
import com.orhanobut.hawk.Hawk;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import java.util.Calendar;
import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import ir.co.ariapardaz.alarm.Activity.Adapter.RcAdepter;
import ir.co.ariapardaz.alarm.Activity.DetailClass;
import ir.co.ariapardaz.alarm.Activity.Model.pcmodel;
import ir.co.ariapardaz.alarm.Activity.Services.MyReceiver;
import ir.co.ariapardaz.alarm.R;


@EActivity(R.layout.activity_period_clock)
public class PeriodClockActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    Context context = this;
    public int idcode;
    final RealmConfiguration pcConfig = new RealmConfiguration.Builder().name("pcrealm.realm")
            .schemaVersion(2).build();
    final Realm realm = Realm.getInstance(pcConfig);
    private RealmResults<pcmodel> result;


    @Click
    void Fab1() {
        AddClockpDialog();
    }

    @ViewById
    RecyclerView period_rc;
    @ViewById
    SwipeRefreshLayout refresh;
    @Bean
    DetailClass DC;

    @AfterViews
    void AV() {


        RcAdepter articleAdapter = new RcAdepter(context);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(context,
                LinearLayoutManager.VERTICAL, false);

        period_rc.setLayoutManager(horizontalLayoutManagaer);
        period_rc.setAdapter(articleAdapter);

        refresh.setOnRefreshListener(this);

    }


    public void PeriodEdit(int X, boolean s) {
        idcode = X;
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                result = realm.where(pcmodel.class).equalTo("id", idcode).findAll();

            }
        });

        EditClockpDialog(result.get(0), s);

    }

    public void PeriodSwitch(int X) {
        idcode = X;

        pcmodel result = realm.where(pcmodel.class).equalTo("id", idcode).findFirst();
        boolean sw = result.isOffon();
        if (sw) {

            realm.beginTransaction();
            result.setOffon(false);
            realm.commitTransaction();
            cancels(X);
        } else {
            realm.beginTransaction();
            result.setOffon(true);
            realm.commitTransaction();
           Start(result.getTimeDetail(), result.getId(), result.getDetail());
        }

    }


    private void AddClockpDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_add, null);
        final EditText edtserial, houe_edt, min_edt, month_edt, day_edt;
        final Switch notif_sw, sound_sw;
        final Spinner SoundSpiner;

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogDanger));
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("ADD", null);
        alertDialogBuilder.setNegativeButton("CANCEL", null);

        min_edt = (EditText) view.findViewById(R.id.min_edt);
        houe_edt = (EditText) view.findViewById(R.id.hour_edt);
        month_edt = (EditText) view.findViewById(R.id.month_edt);
        day_edt = (EditText) view.findViewById(R.id.day_edt);
        edtserial = (EditText) view.findViewById(R.id.edtserial);
        notif_sw = (Switch) view.findViewById(R.id.notif_sw);
        sound_sw = (Switch) view.findViewById(R.id.sound_sw);
        SoundSpiner = (Spinner) view.findViewById(R.id.SoundSpiner);

        ArrayAdapter<String> SoundAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.SoundList));
        SoundAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SoundSpiner.setAdapter(SoundAdapter);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                btnPositive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String mtn1 = "نام دوره وارد نشده";
                        String notTemp = "دوره زمانی:";
                        String SoundMode = SoundSpiner.getSelectedItem().toString();
                        int Zaman = 0;

                        if (!(TextUtils.isEmpty(min_edt.getText()))) {
                            Zaman += Integer.parseInt(min_edt.getText().toString()) * 60;
                            notTemp += min_edt.getText().toString() + " دقیقه ";
                        }
                        if (!(TextUtils.isEmpty(houe_edt.getText()))) {
                            Zaman += Integer.parseInt(houe_edt.getText().toString()) * 3600;
                            notTemp += houe_edt.getText().toString() + " ساعت ";
                        }
                        if (!(TextUtils.isEmpty(day_edt.getText()))) {
                            Zaman += Integer.parseInt(day_edt.getText().toString()) * 86400;
                            notTemp += day_edt.getText().toString() + " روز ";
                        }
                        if (!(TextUtils.isEmpty(month_edt.getText()))) {
                            Zaman += Integer.parseInt(month_edt.getText().toString()) * 2592000;
                            notTemp += month_edt.getText().toString() + " ماه ";
                        }

                        if (!(TextUtils.isEmpty(edtserial.getText())))
                            mtn1 = edtserial.getText().toString();

                        if (Zaman != 0) {
                            int code = getNextKey();

                            DC.ToastTest(SoundMode);
                            save(mtn1, notTemp, SoundMode, Zaman, true, notif_sw.isChecked(), sound_sw.isChecked(), min_edt.getText().toString(),
                                    houe_edt.getText().toString(), day_edt.getText().toString(), month_edt.getText().toString(), code);
                            Start(Zaman, code, mtn1);
                            finish();
                            startActivity(getIntent());
                            alertDialog.dismiss();
                        }
                    }
                });

                Button btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                btnNegative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toasty.warning(context, "دوره زمانی مورد نظر ثبت نشد", Toast.LENGTH_LONG, true).show();
                        alertDialog.dismiss();
                    }
                });
            }
        });
        alertDialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_linear));
        alertDialog.show();
        int width = Hawk.get("width");
        width = (int) ((width) * ((double) 4 / 5));
        alertDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    private void EditClockpDialog(final pcmodel p, final boolean switchoffon) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_add, null);
        final EditText edtserial, houe_edt, min_edt, month_edt, day_edt;
        final Switch notif_sw, sound_sw;
        final int timetemp = p.getTimeDetail();
        final Spinner SoundSpiner;


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogDanger));
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setPositiveButton("EDIT", null);
        alertDialogBuilder.setNegativeButton("DELET", null);

        edtserial = (EditText) view.findViewById(R.id.edtserial);
        houe_edt = (EditText) view.findViewById(R.id.hour_edt);
        min_edt = (EditText) view.findViewById(R.id.min_edt);
        month_edt = (EditText) view.findViewById(R.id.month_edt);
        day_edt = (EditText) view.findViewById(R.id.day_edt);
        notif_sw = (Switch) view.findViewById(R.id.notif_sw);
        sound_sw = (Switch) view.findViewById(R.id.sound_sw);
        SoundSpiner = (Spinner) view.findViewById(R.id.SoundSpiner);

        ArrayAdapter<String> SoundAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.SoundList));
        SoundAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SoundSpiner.setAdapter(SoundAdapter);
        int spinnerPosition = SoundAdapter.getPosition(p.getSoundid());


        edtserial.setText(p.getDetail());
        min_edt.setText(p.getMin());
        houe_edt.setText(p.getHour());
        day_edt.setText(p.getDay());
        month_edt.setText(p.getMonth());
        notif_sw.setChecked(p.isNotif_sw());
        sound_sw.setChecked(p.isSound_sw());
        SoundSpiner.setSelection(spinnerPosition);


        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {


                Button btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                btnPositive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String mtn1 = "نام دوره وارد نشده";
                        String notTemp = "دوره زمانی:";
                        int Zaman = 0;
                        String SoundMode = SoundSpiner.getSelectedItem().toString();

                        if (!(TextUtils.isEmpty(min_edt.getText()))) {
                            Zaman += Integer.parseInt(min_edt.getText().toString()) * 60;
                            notTemp += min_edt.getText().toString() + " دقیقه ";
                        }
                        if (!(TextUtils.isEmpty(houe_edt.getText()))) {
                            Zaman += Integer.parseInt(houe_edt.getText().toString()) * 3600;
                            notTemp += houe_edt.getText().toString() + " ساعت ";
                        }
                        if (!(TextUtils.isEmpty(day_edt.getText()))) {
                            Zaman += Integer.parseInt(day_edt.getText().toString()) * 86400;
                            notTemp += day_edt.getText().toString() + " روز ";
                        }
                        if (!(TextUtils.isEmpty(month_edt.getText()))) {
                            Zaman += Integer.parseInt(month_edt.getText().toString()) * 2592000;
                            notTemp += month_edt.getText().toString() + " ماه ";
                        }

                        if (!(TextUtils.isEmpty(edtserial.getText())))
                            mtn1 = edtserial.getText().toString();


                        if (Zaman != 0) {

                            int code = p.getId();
                            if (switchoffon) {
                                if (Zaman != timetemp) {
                                    cancels(p.getId());
                                    Start(Zaman, code, mtn1);
                                }
                            }
                            EditRealm(mtn1, notTemp + "", SoundMode, Zaman, switchoffon, notif_sw.isChecked(), sound_sw.isChecked(), min_edt.getText().toString(),
                                    houe_edt.getText().toString(), day_edt.getText().toString(), month_edt.getText().toString(), code);
                            finish();
                            startActivity(getIntent());
                            alertDialog.dismiss();
                        }
                    }
                });

                Button btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                btnNegative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        DeletClock(idcode);
                        Toasty.error(context, "دوره زمانی مورد نظر پاک شد", Toast.LENGTH_LONG, true).show();
                        alertDialog.dismiss();
                    }
                });

            }
        });

        alertDialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_linear));
        alertDialog.show();


        int width = Hawk.get("width");
        width = (int) ((width) * ((double) 4 / 5));
        alertDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
    }


    void Start(double Time, int codere, String Mtn) {
        int d = (int) Time;
        int code = codere;
        String mtn2 = Integer.toString(codere);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, d);
        long time = cal.getTimeInMillis();
        Intent i = new Intent(PeriodClockActivity.this, MyReceiver.class).putExtra("mtn", mtn2);
        PendingIntent pi = PendingIntent.getBroadcast(PeriodClockActivity.this, code, i, 0);
        DC.ToastTest(d + "");
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, 1000 * d, pi); // Millisec * Second * Minute


    }


    public void DeletClock(int X) {
        idcode = X;
        cancels(X);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<pcmodel> result = realm.where(pcmodel.class).equalTo("id", idcode).findAll();
                result.deleteAllFromRealm();
            }
        });

        finish();

        startActivity(getIntent());
    }


    void cancels(int cancelcode) {

        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);

        Intent i = new Intent(PeriodClockActivity.this, MyReceiver.class);

        PendingIntent pi = PendingIntent.getBroadcast(PeriodClockActivity.this, cancelcode, i, 0);

        manager.cancel(pi);

    }


    void save(String detail, String note, String soundid, int timedetail, boolean offon, boolean notif_sw,
              boolean sound_sw, String min, String hour, String day, String month, int idcode) {

        realm.beginTransaction();
        final pcmodel model = realm.createObject(pcmodel.class, idcode);
        model.setMin(min);
        model.setHour(hour);
        model.setDay(day);
        model.setMonth(month);
        model.setDetail(detail);
        model.setNote(note);
        model.setSoundid(soundid);
        model.setTimeDetail(timedetail);
        model.setOffon(offon);
        model.setNotif_sw(notif_sw);
        model.setSound_sw(sound_sw);
        realm.commitTransaction();

    }


    void EditRealm(String detail, String note, String soundid, int timedetail, boolean offon, boolean notif_sw,
                   boolean sound_sw, String min, String hour, String day, String month, int idcode) {


        // Realm realm = Realm.getDefaultInstance();


        pcmodel model = realm.where(pcmodel.class).equalTo("id", idcode).findFirst();
        realm.beginTransaction();
        model.setMin(min);
        model.setHour(hour);
        model.setDay(day);
        model.setMonth(month);
        model.setDetail(detail);
        model.setNote(note);
        model.setSoundid(soundid);
        model.setTimeDetail(timedetail);
        model.setOffon(offon);
        model.setNotif_sw(notif_sw);
        model.setSound_sw(sound_sw);
        realm.commitTransaction();

    }


    public int getNextKey() {

        try {
            Number number = realm.where(pcmodel.class).max("id");
            if (number != null) {
                return number.intValue() + 1;
            } else {
                return 0;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }
    }


    @Override
    public void onRefresh() {

        finish();
        startActivity(getIntent());
        refresh.setRefreshing(false);
    }

    @Override
    public void onBackPressed() {

        MainPageActivity_.intent(context).start();

    }
}
