package ir.co.ariapardaz.alarm.Activity.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import ir.co.ariapardaz.alarm.Activity.Activity.PeriodClockActivity;
import ir.co.ariapardaz.alarm.Activity.Model.pcmodel;
import ir.co.ariapardaz.alarm.R;


public class RcAdepter extends RecyclerView.Adapter<RcAdepter.MyViewHolder> {


    Context contextm;
    final RealmConfiguration pcConfig = new RealmConfiguration.Builder().name("pcrealm.realm")
            .schemaVersion(2).build();
    final Realm realm = Realm.getInstance(pcConfig);

    RealmResults<pcmodel> pcmodels = realm.where(pcmodel.class).findAll();

    public RcAdepter(Context context) {
        contextm = context;
    }

    public RcAdepter(List<pcmodel> pcmodels, Context context) {
        this.contextm = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {


        Switch switch1;
        Button edt_btn;
        TextView DetailTxt, TimeDetail;

        public MyViewHolder(View itemView) {
            super(itemView);
            switch1 = (Switch) itemView.findViewById(R.id.switch1);
            edt_btn = (Button) itemView.findViewById(R.id.edt_btn);
            DetailTxt = (TextView) itemView.findViewById(R.id.DetailTxt);
            TimeDetail = (TextView) itemView.findViewById(R.id.TimeDetail);


            edt_btn.setOnClickListener(this);
            switch1.setOnClickListener(this);

        }



        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.edt_btn) {

                int i = pcmodels.get(getAdapterPosition()).getId();
                boolean s=switch1.isChecked();

                ((PeriodClockActivity) contextm).PeriodEdit(i,s);
            }
            if (v.getId() == R.id.switch1) {

                int i = pcmodels.get(getAdapterPosition()).getId();

               ((PeriodClockActivity) contextm).PeriodSwitch(i);
            }
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pcmodel, parent, false);
        final MyViewHolder holder = new MyViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        pcmodel model = pcmodels.get(position);
        holder.DetailTxt.setText(model.getDetail());
        holder.TimeDetail.setText(model.getNote());
        holder.edt_btn.setText("Edit");
        holder.switch1.setChecked(model.isOffon());


        Typeface byekan = Typeface.createFromAsset(contextm.getAssets(), "byekan.ttf");
        Typeface BKOODB = Typeface.createFromAsset(contextm.getAssets(), "BKOODB.ttf");

        holder.DetailTxt.setTypeface(byekan);
        holder.TimeDetail.setTypeface(BKOODB);
    }

    @Override
    public int getItemCount() {
        return pcmodels.size();
    }


}
