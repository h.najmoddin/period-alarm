package ir.co.ariapardaz.alarm.Activity.Adapter;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import ir.co.ariapardaz.alarm.Activity.Activity.EventManualActivity;
import ir.co.ariapardaz.alarm.Activity.Activity.PeriodClockActivity;
import ir.co.ariapardaz.alarm.Activity.Model.EventModel;
import ir.co.ariapardaz.alarm.Activity.Model.pcmodel;
import ir.co.ariapardaz.alarm.R;

import static android.content.Context.ALARM_SERVICE;


public class EventAdepter extends RecyclerView.Adapter<EventAdepter.MyViewHolder> {


    Context contextm;
    final RealmConfiguration eventConfig = new RealmConfiguration.Builder().name("evntrealm.realm")
            .schemaVersion(3).build();
    final Realm Evrealm = Realm.getInstance(eventConfig);

    RealmResults<EventModel> eventModels = Evrealm.where(EventModel.class).findAll();

    public EventAdepter(Context context) {
        contextm = context;
    }

    public EventAdepter(List<EventModel> eventModels, Context context) {
        this.contextm = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        Switch switch1;
        Button edt_btn;
        TextView Event_txt, time_txt;

        public MyViewHolder(View itemView) {
            super(itemView);
            switch1 = (Switch) itemView.findViewById(R.id.switch1);
            edt_btn = (Button) itemView.findViewById(R.id.edt_btn);
            Event_txt = (TextView) itemView.findViewById(R.id.Event_txt);
            time_txt = (TextView) itemView.findViewById(R.id.time_txt);


            edt_btn.setOnClickListener(this);
            switch1.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.edt_btn) {
                int i = eventModels.get(getAdapterPosition()).getId();
                boolean s=switch1.isChecked();
                ((EventManualActivity) contextm).PeriodEdit(i,s);
            }
            if (v.getId() == R.id.switch1) {
                int i = eventModels.get(getAdapterPosition()).getId();
               ((EventManualActivity) contextm).PeriodSwitch(i);
            }
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_eventmodel, parent, false);
        final MyViewHolder holder = new MyViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        EventModel model = eventModels.get(position);
        holder.Event_txt.setText(model.getEvent());
        holder.time_txt.setText(model.getYear() + "/" + model.getMonth() + "/" + model.getDay()+" _ " +model.gethoure()+":00:00");
        holder.edt_btn.setText("Edit");
        holder.switch1.setChecked(model.isOffon());

        Typeface byekan = Typeface.createFromAsset(contextm.getAssets(), "byekan.ttf");
        Typeface BKOODB = Typeface.createFromAsset(contextm.getAssets(), "BKOODB.ttf");

        holder.Event_txt.setTypeface(byekan);
        holder.time_txt.setTypeface(BKOODB);
    }

    @Override
    public int getItemCount() {
        return eventModels.size();
    }

}
