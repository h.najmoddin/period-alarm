package ir.co.ariapardaz.alarm.Activity.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by m on 2018/12/10.
 */

public class pcmodel extends RealmObject {

    @PrimaryKey
    int id;

    String Detail,note,soundid,min,hour,day,month;
    int  TimeDetail;
    boolean offon,notif_sw,sound_sw;

    public pcmodel() {
    }

    public pcmodel(int id, String detail, String note, String soundid, String min, String hour, String day, String month, int timeDetail, boolean offon, boolean notif_sw, boolean sound_sw) {
        this.id = id;
        Detail = detail;
        this.note = note;
        this.soundid = soundid;
        this.min = min;
        this.hour = hour;
        this.day = day;
        this.month = month;
        TimeDetail = timeDetail;
        this.offon = offon;
        this.notif_sw = notif_sw;
        this.sound_sw = sound_sw;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSoundid() {
        return soundid;
    }

    public void setSoundid(String soundid) {
        this.soundid = soundid;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getTimeDetail() {
        return TimeDetail;
    }

    public void setTimeDetail(int timeDetail) {
        TimeDetail = timeDetail;
    }

    public boolean isOffon() {
        return offon;
    }

    public void setOffon(boolean offon) {
        this.offon = offon;
    }

    public boolean isNotif_sw() {
        return notif_sw;
    }

    public void setNotif_sw(boolean notif_sw) {
        this.notif_sw = notif_sw;
    }

    public boolean isSound_sw() {
        return sound_sw;
    }

    public void setSound_sw(boolean sound_sw) {
        this.sound_sw = sound_sw;
    }

    @Override
    public String toString() {
        return "pcmodel{" +
                "id=" + id +
                ", Detail='" + Detail + '\'' +
                ", note='" + note + '\'' +
                ", soundid='" + soundid + '\'' +
                ", min='" + min + '\'' +
                ", hour='" + hour + '\'' +
                ", day='" + day + '\'' +
                ", month='" + month + '\'' +
                ", TimeDetail=" + TimeDetail +
                ", offon=" + offon +
                ", notif_sw=" + notif_sw +
                ", sound_sw=" + sound_sw +
                '}';
    }
}

