package ir.co.ariapardaz.alarm.Activity.Services;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.view.WindowManager;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import ir.co.ariapardaz.alarm.Activity.Model.EventModel;
import ir.co.ariapardaz.alarm.Activity.Model.pcmodel;
import ir.co.ariapardaz.alarm.R;

/**
 * Created by m on 26/09/2019.
 */

public class EventService extends BroadcastReceiver {

    final RealmConfiguration eventConfig = new RealmConfiguration.Builder().name("evntrealm.realm").schemaVersion(3).build();
    final Realm Evrealm = Realm.getInstance(eventConfig);

    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context, "Test time toast", Toast.LENGTH_SHORT).show();




//        int id = Integer.parseInt(intent.getExtras().getString("mtn").toString());
//        f(context, id);
//        ShowNotification(context, id);

    }

    void ShowNotification(Context context, int id) {

//        int i;
//        RealmResults<EventModel> eventModels = Evrealm.where(EventModel.class)
//                .equalTo("id", id)
//                .findAll();


        EventModel model = Evrealm.where(EventModel.class).equalTo("id", id).findFirst();
        // model = eventModels.get(0);

        int SoundMode = R.raw.sound1;
        switch (model.getSoundid()) {

            case ("sound1"):
                SoundMode = R.raw.sound1;
                break;
            case ("sound2"):
                SoundMode = R.raw.sound2;
                break;
            case ("sound3"):
                SoundMode = R.raw.sound3;
                break;
            case ("sound4"):
                SoundMode = R.raw.sound4;
                break;

        }

        if (model.isSound_sw()) {
            MediaPlayer mp = MediaPlayer.create(context, SoundMode);
            mp.setVolume(1, 1);
            mp.start();
        }


        if (model.isNotif_sw()) {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.calendar)
                    .setContentTitle(model.getEvent())
                    .setContentText(model.getYear() + "/" + model.getMonth() + "/" + model.getDay() + " _ " + model.gethoure() + ":00:00")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            notificationManager.notify(id, builder.build());
        }

    }

    public void f(Context context, int id) {

        EventModel model = Evrealm.where(EventModel.class).equalTo("id", id).findFirst();

        if (model.isNotif_sw()) {
            String Detail = model.getEvent();
            String Note = model.getYear() + "/" + model.getMonth() + "/" + model.getDay() + " _ " + model.gethoure() + ":00:00";

            AlertDialog alertDialog = new AlertDialog.Builder(context)
                    .setTitle(Detail)
                    .setMessage(Note)
                    .create();

            alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            alertDialog.show();


        }


    }

}
