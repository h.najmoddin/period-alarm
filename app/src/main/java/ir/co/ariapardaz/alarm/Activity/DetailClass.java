package ir.co.ariapardaz.alarm.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import es.dmoral.toasty.Toasty;


@EBean
public class DetailClass  {

    @RootContext
    Context mContext;

    public void ToastTest(String mtn){
      Toast.makeText(mContext, mtn, Toast.LENGTH_SHORT).show();
    }

    public static void L(String tag , String msg){
        Log.d(tag, "log : " + msg);
    }



}
