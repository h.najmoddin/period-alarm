package ir.co.ariapardaz.alarm.Activity.Activity;


import android.content.Context;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import com.orhanobut.hawk.Hawk;
import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EActivity;
import io.realm.Realm;
import ir.co.ariapardaz.alarm.R;


@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {




    public Context context = this;
    @AfterInject
    void ai() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        Hawk.put("width", width);


        MainPageActivity_.intent(context).start();

    }

}
