package ir.co.ariapardaz.alarm.Activity.Services;


import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.view.WindowManager;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import ir.co.ariapardaz.alarm.Activity.Model.pcmodel;
import ir.co.ariapardaz.alarm.R;

public class MyReceiver extends BroadcastReceiver {

    final RealmConfiguration pcConfig = new RealmConfiguration.Builder().name("pcrealm.realm").schemaVersion(2).build();
    final Realm realm = Realm.getInstance(pcConfig);

    @Override
    public void onReceive(Context context, Intent arg1) {
        Toast.makeText(context, "My reciver terst", Toast.LENGTH_SHORT).show();
        MediaPlayer mp = MediaPlayer.create(context, R.raw.sound2);
        mp.setVolume(1, 1);
        mp.start();

//        int id = Integer.parseInt(arg1.getExtras().getString("mtn").toString());
//        f(context, id);
//        ShowNotification(context, id);
    }


    void ShowNotification(Context context, int id) {

        int i;
        RealmResults<pcmodel> pcmodels = realm.where(pcmodel.class)
                .equalTo("id", id)
                .findAll();

        pcmodel model;
        model = pcmodels.get(0);

        int SoundMode = R.raw.sound1;
        switch (model.getSoundid()) {

            case ("sound1"):
                SoundMode = R.raw.sound1;
                break;
            case ("sound2"):
                SoundMode = R.raw.sound2;
                break;
            case ("sound3"):
                SoundMode = R.raw.sound3;
                break;
            case ("sound4"):
                SoundMode = R.raw.sound4;
                break;

        }

        if (model.isSound_sw()) {
            MediaPlayer mp = MediaPlayer.create(context, SoundMode);
            mp.setVolume(1, 1);
            mp.start();
        }


        if (model.isNotif_sw()) {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.alarm)
                    .setContentTitle(model.getDetail())
                    .setContentText(model.getNote())
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            notificationManager.notify(id, builder.build());
        }

    }

    public void f(Context context, int id) {

        pcmodel model = realm.where(pcmodel.class).equalTo("id", id).findFirst();

        if (model.isNotif_sw()) {
            String Detail = model.getDetail();
            String Note = model.getNote();

            AlertDialog alertDialog = new AlertDialog.Builder(context)
                    .setTitle(Detail)
                    .setMessage(Note)
                    .create();

            alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            alertDialog.show();


        }



    }



}

